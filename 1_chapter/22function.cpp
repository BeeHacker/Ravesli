#include <iostream>

// если define объявлен  "Printing", если нет то "Not printing!"
//#define PRINT

using namespace std;

void doSomething()
{
#ifdef PRINT
 cout<<"Printing";
#endif
#ifndef PRINT
cout<<"Not printing!";
#endif
}
