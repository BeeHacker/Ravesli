#include <iostream>
using namespace std;

int getUserInput()
{
	cout<<"Pleace enter an integer: ";
	int value;
	cin>>value;
	return value;
	}

int getMathematicalOperation()
{
    cout << "Please enter which operator you want (1 = +, 2 = -, 3 = *, 4 = /): ";
 
    int op;
    cin >> op;
 
    // А что, если пользователь введет некорректный символ?
    // Пока что мы это проигнорируем
 
    return op;
}

int calculateResult(int x, int op, int y)
{
    // Обратите внимание, оператор == используется для сравнения двух значений
 
    if (op == 1) // если пользователь выбрал операцию сложения (№1)
        return x + y; // то выполняем эту строку
    if (op == 2) // если пользователь выбрал операцию вычитания (№2)
        return x - y; // то выполняем эту строку
    if (op == 3) // если пользователь выбрал операцию умножения (№3)
        return x * y; // то выполняем эту строку
    if (op == 4) // если пользователь выбрал операцию деления (№4)
        return x / y; // то выполняем эту строку
	
    return -1; // вариант, если пользователь ввел некорректный символ
}
void printResult(int result)
{
    cout << "Your result is: " << result << endl;
}

int main()
{
    // Получить первое значение от пользователя
    int input1=getUserInput();
 
    // Получить математическую операцию от пользователя
    int op=getMathematicalOperation();
 
    // Получить второе значение от пользователя
    int input2=getUserInput();
 
    // Вычислить результат
    int result=calculateResult(input1, op, input2);
 
    // Вывести результат
    printResult(result);
}
