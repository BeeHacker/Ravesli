# Типы ошибок 

* Семантическая ошибка
* Синтаксическая ошибка

# Дебаггер (Debugger)

# Stepping

* Step into (шаг с заходом)
* Step over for Code::Block "Next Line" (Шаг с обходом)
* Step out (шаг с выходом)

```
#include <iostream>
 
void printValue(int nValue)
{
    std::cout << nValue;
}
 
int main()
{
    printValue(5);
    return 0;
}
```

## Выполнить до текущей позиции

* Выполнить до текущей позиции
* Continue (Команда "продолжить") 
* breakpoints (точки остановки) маркер на котором отладчик останавливает процесс выполнения программы.

[https://ravesli.com/urok-26-otladka-programm-stepping-i-breakpoints/]
