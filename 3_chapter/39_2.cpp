#include <iostream>
using namespace std;

int main()
{
    // Переменная count хранит текущее число для вывода 
    int count = 0; // начинаем с 0
 
    // Повторение операции (цикл) до тех пор, пока count не будет равен 100
    while (count <= 200)
    {
        cout << count << " "; // вывод текущего числа 
 
        // Если count делится на 20 без остатка, то вставляем разрыв строки и продолжаем с новой строки
        if (count % 13 == 0)
            cout << "\n";
            else
            if (count%2==0)
            cout<<" d ";
 
        count = count + 1; // переходим к следующему числу
    } // конец while
 
    return 0;
} // конец main()
