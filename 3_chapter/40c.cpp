#include <iostream>
using namespace std;

int add(int x, int y)
{
    return x + y;
}
 
int main()
{
    int x = 5;
    int value = add(x, ++x); 
    // здесь 5 + 6 или 6 + 6?  Это зависит от компилятора и от того, в каком порядке он будет обрабатывать аргументы функции
    
    cout << value; 
    // результатом может быть 11 или 12
 
    return 0;
}
