#include <iostream>
#include <cmath> // подключаем pow()

using namespace std;
 
int main()
{
    cout << "Enter the base: ";
    double base;
    cin >> base;
 
    cout << "Enter the exponent: ";
    double exp;
    cin >> exp;
 
    cout << base << "^" << exp << " = " << pow(base, exp) << "\n";
 
    return 0;
}
