#include <iostream>

using namespace std;

int main()
{
    int x = 4;
    int y = 4;
 
    cout <<x<< " x y " << y << "\n";
    cout <<++x<< " ++x --y " << --y << "\n";
    cout <<x<< " x y " << y<< "\n";
    cout <<x++<< " x++ y-- " << y-- << "\n";
    cout <<x<< " x y " << y << "\n";
    cout <<"bravo!\n";
 
    return 0;
}
