#include "const.h"
#include <iostream>

using namespace std;
using namespace constants;

int main(){
	double radius;
	cout<<"Enter radius:";
	cin>>radius;
	double circumference = 2 * radius * pi;//constants::pi;
	cout<<"Circumference:"<<circumference<<endl;
	return 0;
	}
