#include <iostream>
using namespace std;
int main()
{
    double zero = 0.0;
    double posinf = 5.0 / zero; // положительная бесконечность 
    cout << posinf << "\n";
 
    double neginf = -5.0 / zero; // отрицательная бесконечность 
    cout << neginf << "\n";
 
    double nan = zero / zero; // не число (математически некорректно)
    cout << nan << "\n";
 
    return 0;
}
