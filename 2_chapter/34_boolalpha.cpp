
#include <iostream>
 
using namespace std;
 
int main()
{
    cout << true << endl;
    cout << false << endl;
 
    cout << boolalpha; // выводим логические значения как "true" или "false"
 
    cout << true << endl;
    cout << false << endl;
    return 0;
}
