#include <iostream>

using namespace std;
 
// Возвращаем true, если x и y равны, в противном случае - возвращаем false 
bool isEqual(int x, int y)
{
    return (x == y); // оператор == возвращает true, если x равно y, в противном случае - false 
}
 
int main()
{
    cout << "Enter an integer: ";
    int x;
    cin >> x;
 
    cout << "Enter another integer: ";
    int y;
    cin >> y;
 
    if (isEqual(x, y))
        cout << x << " and " << y << " are equal" << endl;
    else
        cout << x << " and " << y << " are not equal" << endl;
 
    return 0;
}
