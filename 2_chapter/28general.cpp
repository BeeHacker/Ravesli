#include <iostream>

using namespace std;

int main()
{
    // Все переменные в самом верху функции
    int x;
    int y;
 
    // А затем уже весь остальной код
    cout << "Enter a number: ";
    cin >> x;
 
    cout << "Enter another number: ";
    cin >> y;
 
    cout << "The sum is: " << x + y << std::endl;
    return 0;
}
