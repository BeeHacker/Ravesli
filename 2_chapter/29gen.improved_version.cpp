#include <iostream>

using namespace std;

int main()
{
    cout << "Enter a number: ";
    int x; // мы используем x в следующей строке, поэтому объявляем эту переменную здесь (как можно ближе к её первому использованию)
    cin >> x; // первое использование переменной x
 
    cout << "Enter another number: ";
    int y; // переменная y понадобилась нам только здесь, поэтому здесь её и объявляем 
    cin >> y; // первое использование переменной y
 
    cout << "The sum is: " << x + y << std::endl;
    return 0;
}
