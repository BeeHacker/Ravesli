#include <iostream>

using namespace std;

int main(){
unsigned short x=65535;//maximal value a 16-bit unsigned variable can store.
cout<<"x was: "<<x<<endl;
x=x+1;
// 65536 is a number greater than the maximum allowed number in the range of allowed values. Hence, an overflow will occur, since the variable x cannot store 17 bits
cout<<"x is now:"<<x<<endl;
return 0;
}
