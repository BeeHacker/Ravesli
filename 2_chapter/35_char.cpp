#include <iostream>
using namespace std;

int main()
{
    cout << "\"This is quoted text\"\n";
    cout << "This string contains a single backslash \\" << endl;
    cout << "6F in hex is char \'\x6F\'" << endl;
    return 0;
}
