#include <iostream>

using namespace std;

int main(){
int n(5);
cout<<"n was: "<<n<<endl;
double d(5.3);
cout<<"d was:"<<d<<endl;
float f(5.0f);
cout<<"f was:"<<f<<endl;
double d1(5000.0);
cout<<"d1 was:"<<d1<<endl;
double d2(5e3); // другой способ присвоить значение 5000
cout<<"d2 was:"<<d2<<endl;
double d3(0.05);
cout<<"d3 was:"<<d3<<endl;
double d4(5e-2); // другой способ присвоить значение 0.05
cout<<"d4 was:"<<d4<<endl;
return 0;
}
