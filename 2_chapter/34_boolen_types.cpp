
#include <iostream>

using namespace std;
 
int main()
{
    cout << true << endl; // вместо true единица
    cout << !true << endl; // вместо !true ноль
 
    bool b(false);
    cout << b << endl; // b - false (0)
    cout << !b << endl; // !b - true (1)
    return 0;
}
