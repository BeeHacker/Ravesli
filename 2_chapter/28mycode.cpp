#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	bool logic1=true;
	bool logic2=false;
	char symbol1=34;
	char symbol2=33;
	float pi=3.14159;
	double e=2.71828;
	short n=1;
	int n1=64;
	long n3=887846435587664;
	
	//Printing varible
	cout<<"Printing logic1:"<<logic1<<endl;
	cout<<"Printing logic2:"<<logic2<<endl;
	cout<<"Printing symbol1:"<<symbol1<<endl;
	cout<<"Printing symbol2:"<<symbol2<<endl;
	cout<<"Printing pi:"<<pi<<endl;
	cout<<"Printing e:"<<e<<endl;
	cout<<"Printing n:"<<n<<endl;
	cout<<"Printing n1:"<<n1<<endl;
	cout<<"Printing n3:"<<n3<<endl;
	return 0;
}
