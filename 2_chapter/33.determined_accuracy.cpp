#include <iostream>
#include <iomanip> // для setprecision()
using namespace std;
 
int main()
{
    cout << setprecision(16); // задаем точность в 16 цифр
    float f = 3.33333333333333333333333333333333333333f;
    cout << f << endl;
    double d = 3.3333333333333333333333333333333333333;
    cout << d << endl;
    return 0;
}
