#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
	//Прямая инициализация с помощью () 
	int value0(1), nValue0(2);
	cout<<"value0 = "<<value0<<endl;
	cout<<"nValue0 = "<<nValue0<<endl;
	//uniform-инициализация in C++11
	int value{3}, value1{};
	cout<<"value = "<<value<<endl;
	cout<<"value1 = "<<value1<<endl;
	//Копирующее присваивание (обычный С++)
	int nValue;
	nValue = 5;
	cout<<"nValue = "<<nValue<<endl;
	
	return 0;
	}
