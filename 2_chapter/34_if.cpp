#include <iostream>

using namespace std;
 
int main()
{
    cout << "Enter an integer: ";
    int x;
    cin >> x;
 
    if (x == 0)
        cout << "The value is zero" << endl;
    else
        cout << "The value is non-zero" << endl;
    return 0;
}
