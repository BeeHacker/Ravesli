#include <iostream>
#include <iomanip> // для setprecision()
using namespace std;

int main()
{
    cout << setprecision(17);
 
    double d1(1.0);
    cout << d1 << endl;
	
    double d2(0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1); // должно получиться 1.0
    cout << d2 << endl;
}
