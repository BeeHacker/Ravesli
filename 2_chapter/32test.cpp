#include <iostream>
#include <cstdint>

using namespace std;

int main()
{
	//обрабатывает как типа char
    int8_t myint = 65;
    cout << myint << endl;
    //печатает "А" или "65"
    //рекомендуется
    int16_t myint1 = 65;
    cout << myint1 << endl;
 
    return 0;
}
