#include <iostream>
#include <iomanip> // для setprecision()
using namespace std;

int main()
{
	//Потеря данных
    float f(123456789.0f); // переменная f имеет 10 значащих цифр
    cout << setprecision(9); // задаем точность в 9 цифр 
    cout << f << endl;
    return 0;
}
