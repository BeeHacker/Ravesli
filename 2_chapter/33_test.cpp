#include <iostream>
using namespace std;
int main()
{
	float x(3.45e1); cout<<x<<endl;
	float y(4.0e-3); cout<<y<<endl;
	float z(1.23005e2); cout<<z<<endl;
	float a(1.46e5); cout<<a<<endl;
	float b(1.46000001e5); cout<<b<<endl;
	float c(8e-10); cout<<c<<endl;
	return 0;
	}
